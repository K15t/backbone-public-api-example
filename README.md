# Backbone Public Java API Example

This repository is an example project about how to use the public Java API for [Backbone Issue Sync](https://www.k15t.com/software/backbone-issue-sync-for-jira), an issue synchronization app for Jira Server and Jira Cloud.

For now, the capabilities are limited to only provide custom field mappings and you can use the code in this repository as a blueprint for your own custom field mapping.
Simply follow the [Quick Start](#markdown-header-quick-start) steps below which lets you use the provided examples in your own synchronization.
If you want to add Backbone synchronization support to your own Jira app, then follow our tutorial about [creating a custom field mapping from scratch](https://help.k15t.com/backbone-issue-sync/latest/server?contentKey=public-api-tutorial).

**Note**: You can only write custom field mappings for Backbone Issue Sync when it runs and is configured on a **Jira Server** instance.
This applies to synchronizations between Jira Server to Jira Server and between Jira Server to Jira Cloud.
It is important to mention that you can **not** write any Backbone extensions if you're synchronizing Jira Cloud to Jira Cloud!


## Features

* Easy-to-implement interfaces to provide a mapping capability
* Simple and complex field mappings possible

#### Upcoming

* Extend user interface to retrieve user inputs for your field mappings


## Compatibility

Backbone's public Java API can be used with **Backbone 4.0.1** and higher.
We suggest to checkout the master branch of this example and add your examples.
If you use some branches within this project, make sure to check if they are behind the master and update them in case.


## Contents
This readme contains the following chapters:

* [Quick Start](#markdown-header-quick-start) - Start using it.
* [Customizations](#markdown-header-customizations) - Adapt some configuration.
* [Technical Information](#markdown-header-technical-information) - Learn more about the technical background of the API.
* [How-tos](#markdown-header-how-tos) - Learn from existing examples.
* [FAQ](#markdown-header-faq) - Are all your questions answered?
* [Contributions](#markdown-header-contributions) - Contribute to our example project.

## Quick Start

The following steps describe how you can get started writing your custom field mapping for Backbone:

1. Clone this repository: `git clone git@bitbucket.org:K15t/backbone-public-api-example.git`
2. Adapt the custom field mapping examples or implement your own mappings. See also [Customizations](#markdown-header-customizations).
3. Build the app: `mvn package`
4. Upload the app's _*.obr_ file from `/target` to your Jira instance under `Manage Add-ons` (or: `Apps`) using `Upload add-on` (or `app`).

Now you can take a look at the [How-to](#markdown-header-how-tos) section below where you can find examples about different ways how to implement field mappings.

### Configure Maven

In case you are not using this project as an example, make sure to add the following lines to your Maven pom.xml:

```xml
<project>

    <!-- ... -->

    <repositories>
        <repository>
            <id>k15t</id>
            <url>https://nexus.k15t.com/content/repositories/releases</url>
        </repository>
    </repositories>
    
    <dependencies> 
        <!-- ... -->
        <dependency>
            <groupId>com.k15t.backbone</groupId>
            <artifactId>backbone-public-api</artifactId>
            <version>4.0.2</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>
    
    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-jira-plugin</artifactId>
        <configuration> 
            <!-- ... -->
            <instructions>
                <Import-Package>
                    com.k15t.backbone.api.*;version="[4.0.0,5.0.0)",
                    <!-- ... -->
                </Import-Package>
                <!-- ... -->
            </instructions>
            <!-- ... -->
        </configuration>
    </plugin>

</project>
```

As an example you can have a look at the [`pom.xml`](pom.xml) of this project.
Backbone's public Java API was first released with Backbone version 4.0.
With each new Backbone release we'll release a new version of the `backbone-public-api` dependency as well.
However, we don't expect big changes in the public Java API. 

## Customizations

Please consider to take the following requirements into account and update your field mapping and plugin data:

* Customize the key of your plugin artifact by either changing the `key` of the `<atlassian-plugin />` tag in [`atlassian-plugin.xml`](src/main/resources/atlassian-plugin.xml). This key will be used as a prefix for the identifier of your field mapping, see next point.
* Each field mapping implementation must provide a unique identifier by implementing the `getIdentifier()` method of the provided interfaces and choose a meaningful name for `getName()` (see also section [Provided Interfaces](#markdown-header-provided-interfaces))
* Declare your field mappings in [`atlassian-plugin.xml`](src/main/resources/atlassian-plugin.xml) using the tags `<backbone-outgoing-field-mapping />` and `<backbone-incoming-field-mapping />`. Backbone will automatically pick up all field mappings which are declared using these tags.



## Technical Information

In order to get the full potential out of your custom field mapping (and avoid unnecessary questions), we strongly suggest to carefully read the article about the technical concept of our public API.


### Provided Interfaces

At the moment, we provide the following interfaces with which you can provide a custom field mapping, either for the outgoing or incoming side.

|Interface|Description|
|---------|-----------|
|`CustomOutgoingFieldMapping`|Use this interface to create a field mapping for the outgoing side. Provides a `mapOut()` method which must return a value wrapped within a `FieldMappingResponse` object.|
|`CustomIncomingFieldMapping`|Use this interface to create a field mapping for the incoming side. Provides a `mapIn()` method which must return a value wrapped within a `FieldMappingResponse` object.|

TODO: Provide a link to the JavaDocs.

### Provided Services

We do offer a few helper services, like a `JiraRestClient` to access Jira data using HTTP requests or a `LocalRemoteMappingManager` to store relationships between additional entities.
These are further discussed in the examples provided in the [How-tos](#markdown-header-how-tos).

we also provide certain helper classes:

* `StandardJiraFieldTypes`: provides a collection of standard Jira field definitions which you can use to declare the supported fields for your field mapping
* `CustomContextConstants`: contains constants which identify the keys of accessible objects provided by the `context` argument in the field mapping methods `mapOut()` and `mapIn()`
* `JiraRestConstants`: contains constants to help you access Jira's REST API resources if you use the `JiraRestClient`


### Limitations

The public Java API to provide custom field mappings for Backbone covers a lot of use cases for you.
However, you need to be aware that it also has some limitations.
For example, you can not change Backbone's synchronization behaviour in any way.
Backbone will only call your field mapping when it's necessary.
Still, we think that the provided capabilities are enough for a lot of use cases.
If you think we're missing something important, then please reach out to us by using the contact details provided in [About](#markdown-header-about).


## How-tos

In this section we want to highlight a few example use cases how to build a custom field mapping.
Please ensure to read the section about [Technical Information](#markdown-header-technical-information) first before starting to implement any custom field mapping.

### Simple Passthrough Mapping

A passthrough mapping is the simplest mapping which you can implement.
It simply transfers data from A to B without modifying it.
This is really helpful if you want to provide a mapping for a custom field which is available in both projects and can be updated using the REST API.
You can use the base classes `AbstractCustomOutgoingPassthroughMapping` or `AbstractCustomIncomingPassthroughMapping` which are already providing the necessary business logic - you only need to implement the methods to determine the Jira field type and provide a name and identifier.

Please take a look at the examples [Simple Passthrough Mappings](src/main/java/com/k15t/backbone/cfm/example/mappings/simplePassthrough).

### Prefix Mapping

A prefix mapping is similar to a simple passthrough mapping.
It only adds a prefix to the data you're exchanging and hence it's usually applied for mapping text fields.

Please take a look at the example [Prefix Mappings](src/main/java/com/k15t/backbone/cfm/example/mappings/prefixMapping).

### X to Text Mapping

This again is a simple mapping which takes data of a custom field and converts the data into a text format.
It can also be referred to as "object to string" mapping.
This is usually added as an mapping at the outgoing side.
The important difference to the previous mappings is that you need to have different `JiraFieldTypes` for the methods `getJiraFieldType()` and `getMessageFieldType()`.
The field type for the Jira field should match the field which you want to convert into a text format.
The field type for the message should refer to a textfield, e.g. `StandardJiraFieldTypes.STRING_TEXTFIELD`.
Taking this approach, you don't need to reinvent the wheel by implementing an incoming field mapping to read data from the issue synchronization message and add it to a text field on the other side.
The users will be able to reuse existing text mappings from Backbone itself which can already apply text data from the issue synchronization message into a Jira field.

Please take a look at the example [X to Text Mapping](src/main/java/com/k15t/backbone/cfm/example/mappings/objectToTextMapping/ObjectToTextOutgoingMapping.java).

### Accessing Jira Data

In some cases it might be necessary to access further Jira data, e.g. in order to decide how to map a field's value.
For these cases Backbone provides a `JiraRestClient` which you can use to access the Jira instance using Backbone's connection settings.
Therefore you need to implement the `isUsingJiraClient()` method in the field mapping interfaces.
However, you'll only be able to access the `JiraRestClient` if the end user grants permission to your field mapping to request the data.

Please take a look at the example [Conditional Mapping](src/main/java/com/k15t/backbone/cfm/example/mappings/conditionalMapping) which maps a value based on some other Jira data.

### Accessing Issue Relationships

In some rare cases it might be helpful if your field mapping knows to which issue the current issue belongs to in the other project.
You can get this information by using the `CustomSyncInfoManager` to retrieve the sync information based on the local or remote issue key.

Please take a look at the example [Issue Relationship Mapping](src/main/java/com/k15t/backbone/cfm/example/mappings/issueRelationMapping). 


## FAQ

### Can I change the UI of the field mapping definition?
This is not possible yet.
We're working on a proper solution, so that you can request dynamic data from the users of your custom field mapping.

### How can I develop a field mapping and make updates to it?
A field mapping works like a usual Jira plugin: You build a Java app which is shipped as a JAR or OBR file and can be uploaded to Jira in the **Manage Add-ons** section.
We recommend to check out the [Atlassian SDK tutorial](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/) how to create an app for Jira/Confluence.
For updating your field mapping, simply update your code, generate a new OBR file and upload it to Jira again.

### I've uninstalled my field mapping, but Backbone still displays it in the field mapping configuration. How can I remove it?
The reason is that Backbone is caching all custom field mappings.
This provides a better performance for your Jira system and your users.
In case you want to empty the cache, simply open your synchronization configuration, select the upper right three dots **...** -> **Clear Backbone cache**.


## Contributions

You want to contribute?
That's great!
We always appreciate help in the documentation. This can include typos, better explanations or further examples how to use Backbone's public Java API.
We encourage you to create a pull request against the master branch of this project, so we can easily integrate your contribution!

## About

K15t GmbH - https://www.k15t.com

Do you have any questions or suggestions?
Visit our [community forum](https://k15t.zendesk.com/hc/en-us/community/topics/115000034706-Backbone-Issue-Sync) or create a [support ticket](https://k15t.zendesk.com/hc/en-us/requests/new).

## License

The MIT License

Copyright 2019, K15t GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.