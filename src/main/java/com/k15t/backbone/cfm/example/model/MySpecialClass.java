package com.k15t.backbone.cfm.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.k15t.backbone.cfm.example.mappings.objectToTextMapping.ObjectToTextOutgoingMapping;


/**
 * A sample class to showcase an {@link ObjectToTextOutgoingMapping}.
 */
public class MySpecialClass {

    @JsonProperty
    private String title;
    @JsonProperty
    private String data;


    public MySpecialClass() {
        // provide an empty constructor for Jackson
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getData() {
        return data;
    }


    public void setData(String data) {
        this.data = data;
    }
}
