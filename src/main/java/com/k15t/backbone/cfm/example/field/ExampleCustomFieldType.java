package com.k15t.backbone.cfm.example.field;

import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.StandardJiraFieldTypes;


/**
 * This class adds a custom textfield to Jira.
 * This textfield can be mapped via {@link com.k15t.backbone.cfm.example.mappings.simplePassthrough.SimplePassthroughOutgoingMapping} and
 * {@link com.k15t.backbone.cfm.example.mappings.simplePassthrough.SimplePassthroughIncomingMapping}.
 */
@Scanned
public class ExampleCustomFieldType extends GenericTextCFType {

    /**
     * This constant declares the type information about the custom field this app is adding so that the custom field mappings can act on
     * it.
     */
    public static JiraFieldType TYPE =
            new JiraFieldType(StandardJiraFieldTypes.STRING.getType(), "com.k15t.backbone.backbone-public-api-example:examplecustomfield");


    protected ExampleCustomFieldType(
            @JiraImport CustomFieldValuePersister customFieldValuePersister,
            @JiraImport GenericConfigManager genericConfigManager,
            @JiraImport TextFieldCharacterLengthValidator textFieldCharacterLengthValidator,
            @JiraImport JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
    }
}
