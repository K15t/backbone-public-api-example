package com.k15t.backbone.cfm.example.mappings.complexRelationMapping;

import com.k15t.backbone.api.cfm.CustomIncomingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.context.CustomContextConstants;
import com.k15t.backbone.api.cfm.context.LocalRemoteMapping;
import com.k15t.backbone.api.cfm.context.LocalRemoteMappingManager;
import com.k15t.backbone.cfm.example.model.ComplexEntityAttachmentBean;
import com.k15t.backbone.cfm.example.model.ComplexEntityBean;

import java.util.List;
import java.util.Map;


/**
 * This mapping shows how to use the {@link LocalRemoteMappingManager} to store relationships between additional entities. Take this as an
 * example: your custom field stores usual text data as well as a list of attachments. In this case the attachments are not handled by
 * Jira itself, but internally by the custom plugin. If you want to keep track of these attachments (e.g. if attachments are updated, you
 * might want to update the correct attachment on the other side as well), you need to store these attachment relationships somewhere.
 * For this case you can use the {@link LocalRemoteMappingManager} which provides you with a way to store these relationships between any
 * kind of exchanged entities.
 *
 * <strong>Important Note:</strong> You do not need to manage the relationship for a usual custom field. This is already handled by
 * Backbone. Only use the {@link LocalRemoteMappingManager} in case you need to store relationships between additional(!) entities.
 * However, if you want to get the relationship of the local and remote issue, you can use the
 * {@link com.k15t.backbone.api.cfm.context.CustomSyncInfoManager} to determine the issue key from the other side.
 */
public class ComplexEntityIncomingMapping implements CustomIncomingFieldMapping {


    @Override
    public FieldMappingResponse mapIn(SimpleIssueBean simpleIssueBean, Field field, Map<String, Object> context) {
        // First access the sync info manager from the context object
        LocalRemoteMappingManager localRemoteMappingManager =
                (LocalRemoteMappingManager) context.get(CustomContextConstants.LOCAL_REMOTE_MAPPING_MANAGER);

        // retrieve entity and its additional entity, e.g. attachments (see example use case above in class JavaDocs)
        ComplexEntityBean value = field.getValue(ComplexEntityBean.class);
        List<ComplexEntityAttachmentBean> remoteAttachments = value.getAttachments();


        // now iterate over all remote (= incoming) attachments from the other side and store/update the relationship or attachment data
        for (ComplexEntityAttachmentBean remoteAttachment : remoteAttachments) {
            LocalRemoteMapping mapping = localRemoteMappingManager.getMappingByRemoteId(simpleIssueBean.getKey(), remoteAttachment.getId());
            if (mapping == null) {
                // remoteAttachment is new, we need to process it and store the relation ship
                String localAttachmentId = "<retrieve me somehow>";

                // store relationship of local and remote attachment
                localRemoteMappingManager.saveMapping(simpleIssueBean.getKey(), localAttachmentId, remoteAttachment.getId());
            } else {
                // we already know the relationship - determine if you need to update the attachment

                // use the localId to e.g. retrieve the local attachment and compare it with the remote attachment -> has it changed?
                String localId = mapping.getLocalId();

                // update data?
            }
        }

        // null is ignored by Backbone - since this is just an example mapping, you might want to adjust this to your needs
        return null;
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Complex Entity Incoming Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "complexEntityIncomingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return ComplexEntityOutgoingMapping.FIELD_TYPE;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        return ComplexEntityOutgoingMapping.FIELD_TYPE;
    }
}
