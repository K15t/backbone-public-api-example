package com.k15t.backbone.cfm.example.mappings.conditionalMapping;

import com.k15t.backbone.api.cfm.CustomIncomingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.StandardJiraFieldTypes;
import com.k15t.backbone.api.cfm.context.CustomContextConstants;
import com.k15t.backbone.api.rest.JiraRestClient;

import java.util.Map;


/**
 * This example showcases how you can retrieve other data from Jira and base your decision on the retrieved data.
 * <p>
 * <strong>VERY IMPORTANT:</strong> If you want to access {@link JiraRestClient}, you have to override the method
 * {@link #isUsingJiraClient()} and return <code>true</code>. Otherwise the {@link JiraRestClient} instance from the <code>context</code>
 * map will always be null! If your mapping overwrites {@link #isUsingJiraClient()} and returns <code>true</code>, then the user will see
 * a checkbox in Backbone's UI where he can select if he wants to give you access to Backbone's connection settings. The Backbone
 * connection settings are hidden for your field mapping, but the instance of {@link JiraRestClient} will forward your requests to
 * Backbone's internal Jira client which will execute them.
 * <p>
 * <strong>Note:</strong> This example mapping below does not make sense at all. It should only showcase how to retrieve data from Jira
 * by using the {@link JiraRestClient}.
 */
public class ConditionalStringIncomingMapping implements CustomIncomingFieldMapping {


    @Override
    public FieldMappingResponse mapIn(SimpleIssueBean simpleIssueBean, Field field, Map<String, Object> context) {
        // Get a JiraRestClient to access Jira's REST API - you'll only get access to that Jira where the mapping is applied to.
        // For example, if the incoming mapping is executed on Jira B (you apply text data from the sync message which is coming from
        // Jira A), then you'll only be able to access Jira B using this JiraRestClient.
        JiraRestClient jiraRestClient = (JiraRestClient) context.get(CustomContextConstants.JIRA_REST_CLIENT);

        // take care that simpleIssueBean might be null if the issue has not been created by Backbone so far, i.e. we're in the creation
        // process
        if (simpleIssueBean != null) {
            // now call the jiraRestClient to retrieve data from Jira, in this case we simply ask the /serverInfo endpoint
            // NOTE: you can only retrieve data from the Jira instance where this mapping is currently executed!
            // You never get access to "the other" Jira instance!
            Map<String, Object> serverInfo = (Map<String, Object>) jiraRestClient.get("/rest/api/2/serverInfo", Map.class);

            // as an example: let's check for the deployment type and return a different result based on the type
            if (serverInfo.containsKey("deploymentType") && "server".equalsIgnoreCase((String) serverInfo.get("deploymentType"))) {
                // do something, e.g. adjust data which is stored for new issue
                String value = field.getValue(String.class);
                return new FieldMappingResponse("Server Data: " + value);
            } else {
                // do something else, e.g. passthrough value
                return new FieldMappingResponse(field.getValue());
            }
        }

        // null is ignored by Backbone
        return null;
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Conditional String Incoming Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "conditionalStringIncomingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }


    @Override
    public boolean isUsingJiraClient() {
        // It is important to implement this method and return 'true' to be able to retrieve the JiraRestClient!!
        return true;
    }
}
