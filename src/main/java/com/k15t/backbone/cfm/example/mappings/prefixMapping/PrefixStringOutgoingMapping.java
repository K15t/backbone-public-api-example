package com.k15t.backbone.cfm.example.mappings.prefixMapping;

import com.k15t.backbone.api.cfm.CustomOutgoingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.StandardJiraFieldTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


/**
 * An example mapping to provide a string as an outgoing value for a text field. It adds a prefix to the string which is provided to the
 * {@link #mapOut(SimpleIssueBean, Field, Map)} method.
 */
public class PrefixStringOutgoingMapping implements CustomOutgoingFieldMapping {

    private static final Logger logger = LoggerFactory.getLogger(PrefixStringOutgoingMapping.class);

    private static final String PREFIX = "PREFIX_";


    @Override
    public FieldMappingResponse mapOut(SimpleIssueBean simpleIssueBean, Field field, Map<String, Object> context) {
        logger.debug("Mapping out data for prefix mapping...");

        String someTextValue = field.getValue(String.class);
        if (someTextValue != null) {
            return new FieldMappingResponse(PREFIX + someTextValue);
        }

        // in case of null the response is ignored by Backbone
        return null;
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Outgoing Prefix String Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "outgoingPrefixStringMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }
}
