package com.k15t.backbone.cfm.example.mappings.simplePassthrough;

import com.k15t.backbone.api.cfm.AbstractCustomOutgoingPassthroughMapping;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.cfm.example.field.ExampleCustomFieldType;


/**
 * This is an example passthrough mapping for the outgoing side for {@link ExampleCustomFieldType} custom fields. It extends the provided
 * abstract class which will simply take the value from the issue data and use the field data without any modification (hence
 * 'passthrough'). Then, Backbone will take the data and adds it to the sync message, so that the incoming side can pick it up. This is
 * convenient if you have certain custom fields which are simply text or number fields, but don't match Jira's standard field types, because
 * they are added by a custom field app (e.g. by an app from the marketplace).
 * <p>
 * The only TODO for your is to override the methods below to identify this mapping and assign the correct field types to it, so Backbone
 * can match the incoming and outgoing mappings.
 *
 * @see SimplePassthroughIncomingMapping
 */
public class SimplePassthroughOutgoingMapping extends AbstractCustomOutgoingPassthroughMapping {

    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Simple Passthrough Outgoing Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "simplePassthroughOutgoingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return ExampleCustomFieldType.TYPE;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        return ExampleCustomFieldType.TYPE;
    }
}
