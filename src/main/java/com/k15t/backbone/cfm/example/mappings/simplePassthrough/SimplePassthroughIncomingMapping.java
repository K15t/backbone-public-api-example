package com.k15t.backbone.cfm.example.mappings.simplePassthrough;

import com.k15t.backbone.api.cfm.AbstractCustomIncomingPassthroughMapping;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.cfm.example.field.ExampleCustomFieldType;


/**
 * This is an example passthrough mapping for the incoming side for {@link ExampleCustomFieldType} custom fields. It extends the provided
 * abstract class which will simply take the value from the sync message and use the field data without any modification (hence
 * 'passthrough'). Then, Backbone will take the data and apply it to the issue. This is convenient if you have certain custom fields which
 * are simply text or number fields, but don't match Jira's standard field types, because they are added by a custom field app (e.g. by
 * an app from the marketplace).
 * <p>
 * The only TODO for your is to override the methods below to identify this mapping and assign the correct field types to it, so Backbone
 * can match the incoming and outgoing mappings.
 *
 * @see SimplePassthroughOutgoingMapping
 */
public class SimplePassthroughIncomingMapping extends AbstractCustomIncomingPassthroughMapping {


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Simple Passthrough Incoming Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "simplePassthroughIncomingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return ExampleCustomFieldType.TYPE;
    }


    /**
     * We're taking the field type from {@link SimplePassthroughOutgoingMapping}, so the {@link SimplePassthroughOutgoingMapping} can take
     * this {@link JiraFieldType} as well for method {@link #getMessageFieldType()} and Backbone makes sure that only these two mappings
     * with the same field type are matched to each other.
     */
    @Override
    public JiraFieldType getMessageFieldType() {
        return ExampleCustomFieldType.TYPE;
    }
}
