package com.k15t.backbone.cfm.example.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.k15t.backbone.cfm.example.mappings.complexRelationMapping.ComplexEntityIncomingMapping;
import com.k15t.backbone.cfm.example.mappings.complexRelationMapping.ComplexEntityOutgoingMapping;

import java.util.List;


/**
 * A sample bean to showcase {@link ComplexEntityOutgoingMapping} and {@link ComplexEntityIncomingMapping}.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComplexEntityBean {

    @JsonProperty
    private String id;
    @JsonProperty
    private String someText;
    @JsonProperty
    private List<ComplexEntityAttachmentBean> attachments;


    public ComplexEntityBean() {
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getSomeText() {
        return someText;
    }


    public void setSomeText(String someText) {
        this.someText = someText;
    }


    public List<ComplexEntityAttachmentBean> getAttachments() {
        return attachments;
    }


    public void setAttachments(List<ComplexEntityAttachmentBean> attachments) {
        this.attachments = attachments;
    }


    @Override
    public String toString() {
        return "ComplexEntityBean{" +
                "id='" + id + '\'' +
                '}';
    }
}
