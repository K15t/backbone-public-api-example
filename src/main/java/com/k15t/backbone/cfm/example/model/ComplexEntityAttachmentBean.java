package com.k15t.backbone.cfm.example.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.k15t.backbone.cfm.example.mappings.complexRelationMapping.ComplexEntityIncomingMapping;
import com.k15t.backbone.cfm.example.mappings.complexRelationMapping.ComplexEntityOutgoingMapping;


/**
 * A sample bean to showcase {@link ComplexEntityOutgoingMapping} and {@link ComplexEntityIncomingMapping}.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComplexEntityAttachmentBean {

    @JsonProperty
    private String id;
    @JsonProperty
    private String filename;
    @JsonProperty
    private Object data;
    @JsonProperty
    private int size;


    public ComplexEntityAttachmentBean() {
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getFilename() {
        return filename;
    }


    public void setFilename(String filename) {
        this.filename = filename;
    }


    public Object getData() {
        return data;
    }


    public void setData(Object data) {
        this.data = data;
    }


    public int getSize() {
        return size;
    }


    public void setSize(int size) {
        this.size = size;
    }


    @Override
    public String toString() {
        return "ComplexEntityAttachmentBean{" +
                "id='" + id + '\'' +
                ", filename='" + filename + '\'' +
                '}';
    }
}
