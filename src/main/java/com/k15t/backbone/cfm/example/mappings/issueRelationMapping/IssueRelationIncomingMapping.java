package com.k15t.backbone.cfm.example.mappings.issueRelationMapping;

import com.k15t.backbone.api.cfm.CustomIncomingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.StandardJiraFieldTypes;
import com.k15t.backbone.api.cfm.context.CustomContextConstants;
import com.k15t.backbone.api.cfm.context.CustomSyncInfo;
import com.k15t.backbone.api.cfm.context.CustomSyncInfoManager;

import java.util.Map;


/**
 * This example mapping shows how to use the {@link CustomSyncInfoManager} and retrieve the relationship information of the local and
 * remote issue. The remote issue is the issue in the other project whereas the local issue is the one which is created in the local
 * project. (Of course this view is always dependent on where this incoming mapping is executed)
 */
public class IssueRelationIncomingMapping implements CustomIncomingFieldMapping {

    @Override
    public FieldMappingResponse mapIn(SimpleIssueBean simpleIssueBean, Field field, Map<String, Object> context) {
        // take care that the issue bean can be null if the issue has not been created yet, i.e. we're in the creation process
        if (simpleIssueBean != null) {
            // now retrieve the CustomSyncInfoManager from the context
            CustomSyncInfoManager syncInfoManager = (CustomSyncInfoManager) context.get(CustomContextConstants.CUSTOM_SYNC_INFO_MANAGER);

            // with the syncInfoManager we're able to retrieve the sync relationship of the local and its remote issue.
            // this relationship is managed by Backbone and you can only read it.
            CustomSyncInfo syncInfo = syncInfoManager.getSyncInformation(simpleIssueBean.getKey());

            // now you can store the remote issue key somewhere, e.g. in a custom field or somewhere else.
            String remoteIssueKey = syncInfo.getRemoteIssueKey();

            // in this example, we're applying it to a textfield (based on the field type definitions below)
            return new FieldMappingResponse("Local Issue: " + simpleIssueBean.getKey() + ", Remote Issue: " + remoteIssueKey);
        }

        return null;
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Issue Relation Incoming Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "issueRelationIncomingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        // just an example field type as this mapping should just show how to work with the CustomSyncInfoManager
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        // just an example field type as this mapping should just show how to work with the CustomSyncInfoManager
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }
}
