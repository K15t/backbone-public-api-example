package com.k15t.backbone.cfm.example.mappings.complexRelationMapping;

import com.k15t.backbone.api.cfm.CustomOutgoingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.context.LocalRemoteMappingManager;
import com.k15t.backbone.cfm.example.model.ComplexEntityBean;

import java.util.Map;


/**
 * This mapping shows together with {@link ComplexEntityIncomingMapping} how to use {@link LocalRemoteMappingManager}. This can be used
 * to store additional relationships between custom entities which are stored within your custom field.
 *
 * <strong>Important Note:</strong> You do not need to manage the relationship for a usual custom field or its issue. This is already
 * handled by Backbone. Only use the {@link LocalRemoteMappingManager} in case you need to store relationships between additional(!)
 * entities. However, if you want to get the relationship of a local and remote issue, you can use the
 * {@link com.k15t.backbone.api.cfm.context.CustomSyncInfoManager} to determine the issue key from the other side.
 */
public class ComplexEntityOutgoingMapping implements CustomOutgoingFieldMapping {

    public static final JiraFieldType FIELD_TYPE = new JiraFieldType("com.example.complex.entity");


    @Override
    public FieldMappingResponse mapOut(SimpleIssueBean simpleIssueBean, Field field, Map<String, Object> context) {
        // retrieve entity and its additional entity, e.g. attachments (see example use case above in class JavaDocs)
        ComplexEntityBean value = field.getValue(ComplexEntityBean.class);

        // prepare value for sync message if necessary

        // return to Backbone, so it will be added to the sync message
        return new FieldMappingResponse(value);
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Complex Entity Outgoing Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "complexEntityOutgoingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return FIELD_TYPE;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        return FIELD_TYPE;
    }
}
