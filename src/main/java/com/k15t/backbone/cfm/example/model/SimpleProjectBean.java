package com.k15t.backbone.cfm.example.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;


/**
 * A simple bean class for a Jira project.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleProjectBean {

    private String key;
    private String name;
    private String projectTypeKey;


    public SimpleProjectBean() {
    }


    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }


    public String getName() {
        return name;
    }


    // an example to showcase that custom Jackson annotations also work
    @JsonSetter
    public void configureName(String name) {
        this.name = name;
    }


    public String getProjectTypeKey() {
        return projectTypeKey;
    }


    public void setProjectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
    }


    @Override
    public String toString() {
        return "SimpleProjectBean{" +
                "key='" + key + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
