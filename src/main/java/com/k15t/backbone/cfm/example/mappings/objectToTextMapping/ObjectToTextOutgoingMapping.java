package com.k15t.backbone.cfm.example.mappings.objectToTextMapping;

import com.k15t.backbone.api.cfm.CustomOutgoingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.StandardJiraFieldTypes;
import com.k15t.backbone.cfm.example.model.MySpecialClass;

import java.util.Map;


/**
 * This is an example mapping to showcase how you can easily provide an "X to text" mapping for all kinds of custom fields.
 */
public class ObjectToTextOutgoingMapping implements CustomOutgoingFieldMapping {

    @Override
    public FieldMappingResponse mapOut(SimpleIssueBean simpleIssueBean, Field field, Map<String, Object> map) {
        // first, retrieve the field's value based on the data you need
        MySpecialClass value = field.getValue(MySpecialClass.class);

        // Here you can try to translate your object into some sort of text / string, so it's readable for users
        String result = "<Please fill me with data from the object>";

        // afterwards, you can use the resulting string and return it to Backbone, so it'll be added to the sync message
        return new FieldMappingResponse(result);
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Object To Text Outgoing Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "objectToTextOutgoingMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return new JiraFieldType("com.example.my.special.field.type");
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        // This example is using an "X to text" mapping and hence needs to define a text field as the message field type.
        // You might want to change that, e.g. to a number field in case you are returning a number in your mapOut method.
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }
}
