package com.k15t.backbone.cfm.example.mappings.prefixMapping;

import com.k15t.backbone.api.cfm.CustomIncomingFieldMapping;
import com.k15t.backbone.api.cfm.Field;
import com.k15t.backbone.api.cfm.FieldMappingResponse;
import com.k15t.backbone.api.cfm.JiraFieldType;
import com.k15t.backbone.api.cfm.SimpleIssueBean;
import com.k15t.backbone.api.cfm.StandardJiraFieldTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


/**
 * An example mapping to store a string into a text field. It adds a prefix to the string which is provided to the
 * {@link #mapIn(SimpleIssueBean, Field, Map)} method.
 */
public class PrefixStringIncomingMapping implements CustomIncomingFieldMapping {

    private static final Logger logger = LoggerFactory.getLogger(PrefixStringIncomingMapping.class);

    private static final String PREFIX = "PREFIX_";


    @Override
    public FieldMappingResponse mapIn(SimpleIssueBean simpleIssueBean, Field incomingValue, Map<String, Object> context) {
        logger.debug("Mapping in data for prefix mapping...");

        // manipulate the value and return a response
        if (incomingValue != null) {
            return new FieldMappingResponse(PREFIX + incomingValue.getValue());
        }

        // in case of null the response is ignored by Backbone
        return null;
    }


    @Override
    public String getName() {
        // use an appropriate name as this is shown in the Backbone UI of a field mapping
        return "Incoming Prefix String Mapping";
    }


    @Override
    public String getIdentifier() {
        // make sure that this identifier is unique for your plugin! Backbone will auto-prefix it using your plugin key
        return "incomingPrefixStringMapping";
    }


    @Override
    public JiraFieldType getJiraFieldType() {
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }


    @Override
    public JiraFieldType getMessageFieldType() {
        return StandardJiraFieldTypes.STRING_TEXTFIELD;
    }

}
